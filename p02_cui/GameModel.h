//
//  GameModel.h
//  p02_cui
//
//  Created by SHILEI CUI on 2/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GameModel;

@protocol DataDelegate <NSObject>

@optional

- (void)data:(GameModel *)data;

@end

@interface GameModel : NSObject

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger currentScore;
@property (nonatomic, assign) NSInteger bestScore;

@property (nonatomic, weak) id<DataDelegate> delegate;


- (void)addData;

- (void)restart;

- (void)swipeUp;
- (void)swipeDown;
- (void)swipeLeft;
- (void)swipeRight;


@end
