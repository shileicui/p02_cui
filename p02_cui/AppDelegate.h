//
//  AppDelegate.h
//  p02_cui
//
//  Created by SHILEI CUI on 2/3/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

