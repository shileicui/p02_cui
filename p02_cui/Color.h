//
//  UIColor.h
//  p02_cui
//
//  Created by SHILEI CUI on 2/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Color : NSObject

+ (UIColor *)backgroundColor;

+ (UIColor *)singleBoardColor;

+ (UIColor *)gameBoardColor;

+ (UIColor *)buttonColor;

/** The name of the bold font. */
+ (NSString *)boldFontName;

/** The name of the regular font. */
+ (NSString *)regularFontName;

@end
