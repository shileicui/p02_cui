//
//  UIColor.m
//  p02_cui
//
//  Created by SHILEI CUI on 2/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Color.h"


#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]

@implementation Color

+ (UIColor *)backgroundColor
{
    return RGB(250, 248, 239);
}


+ (UIColor *)singleBoardColor
{
    return RGB(204, 192, 179);
}


+ (UIColor *)gameBoardColor
{
    return RGB(187, 173, 160);
}


+ (UIColor *)buttonColor
{
    return RGB(119, 110, 101);
}

+ (NSString *)boldFontName
{
    return @"AvenirNext-DemiBold";
    
}


+ (NSString *)regularFontName
{
    return @"AvenirNext-Regular";
}

@end
