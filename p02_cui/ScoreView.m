//
//  ScoreView.m
//  p02_cui
//
//  Created by SHILEI CUI on 2/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "ScoreView.h"
#import "Color.h"

@implementation ScoreView

- (void)updateAppearance
{
    self.layer.cornerRadius = 6;
    self.layer.masksToBounds = YES;
    self.backgroundColor = [Color buttonColor];
    self.title.font = [UIFont fontWithName:[Color boldFontName] size:12];
    self.score.font = [UIFont fontWithName:[Color regularFontName] size:16];
}

-(void)updateWithScore:(NSInteger)score
{
    NSString *scoreString = [[NSString alloc] initWithFormat:@"%ld", score];
    self.score.text = scoreString;
    
}



@end
