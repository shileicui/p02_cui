
//
//  ScoreView.h
//  p02_cui
//
//  Created by SHILEI CUI on 2/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreView : UIView

@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel *score;

-(void)updateAppearance;
-(void)updateWithScore: (NSInteger)score;

@end
